# group-website
Repo for learning how to make websites with GitLab pages

## Learning Markdown

Vanilla text may contain *italics* and **bold words**.

This paragraph is separated from the previous one by a blank line.
Line breaks
are caused by two trailing spaces at the end of a line.

[Carpentries Webpage](https://carpentries.org/)

### Carpentries Lesson Programs:
- Software Carpentry
- Data Carpentry
- Library Carpentry

## Exercies
~~I don't like to learn new things.~~

### Some more tasks

1. Task 1
2. Task 2
3. Task 3

¹[logo](https://github.com/carpentries/carpentries.org/blob/main/images/TheCarpentries-opengraph.png)
